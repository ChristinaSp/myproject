﻿using MainProject.Properties;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MainProject
{
    class Program
    {
        private static Timer _timerThread;

        private static void ScrapPage()
        {
            IWebDriver driver = new ChromeDriver(@"D:\Chris\Projects\5курс\chromedriver_win32"); //@"D:\Chris\Projects\5курс\chromedriver_win32"
            driver.Url = "https://tsn.ua/news";// "https://www.bbc.com/news";
            var str = driver.PageSource;


            var news = new List<News>();
            var texts = driver.FindElements(By.CssSelector(".p-name.c-post-title.js-ellipsis"));
            var times = driver.FindElements(By.CssSelector(".dt-published.c-post-time"));

            for (int i = 0; i < texts.Count; i++)
            {
                var exactTime = DateTime.Parse(times[i].GetAttribute("datetime"));
                var linkElement = driver.FindElement(By.LinkText(texts[i].Text));
                var link = linkElement.GetAttribute("href");
                var boolv = (Settings.Default.LastDate < exactTime);
                if (link != "https://tsn.ua/news#main-content" && (Settings.Default.LastDate < exactTime))
                    news.Add(new News(texts[i].Text, exactTime, link));
            }
            driver.Close();

            Console.WriteLine("-------------------------------------------------------------------------------");
            if (news.Any())
            {
                Settings.Default.LastDate = news.FirstOrDefault().Time;
                Settings.Default.Save();
                foreach (var item in news)
                {
                    Console.WriteLine(item);
                }
            }
            //Console.WriteLine(str);

            if (news != null)
            {
                string json = JsonConvert.SerializeObject(news, Formatting.Indented);
                SendData(json);
            }
        }
        static void Main(string[] args)
        {
            _timerThread = new Timer((o) =>
            {
                ScrapPage();

            }, null, 0, 10000);

            Console.ReadLine();


        }
        private static void SendData(string data)
        {
            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var body = Encoding.UTF8.GetBytes(data);

                channel.BasicPublish(exchange: "",
                                     routingKey: "hello",
                                     basicProperties: null,
                                     body: body);
                // Console.WriteLine(" [x] Sent {0}", data);
            }


        }
    }
}
