﻿using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsGraphQl.Graphql 
{
    public class NewsInputType : InputObjectGraphType
    {
        public NewsInputType()
        {
            Name = "NewsInput";
            Field<NonNullGraphType<StringGraphType>>("id"); 
            Field<StringGraphType>("title");
            Field<StringGraphType>("link");
            Field<DateGraphType>("time");
            Field<BooleanGraphType>("like");
        }
    }

}
