﻿using GraphQL.Types;
using NewsGraphQl.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsGraphQl.Graphql.Type
{
	class NewsType : ObjectGraphType<News>
	{
		public NewsType()
		{
			Field(x => x.Id);
			Field(x => x.title);
			Field(x => x.link);
			Field(x => x.time);
			Field(x => x.like);
		}
	}

}
