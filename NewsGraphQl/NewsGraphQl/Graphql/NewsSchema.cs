﻿using GraphQL;
using GraphQL.Types;

namespace NewsGraphQl.Graphql
{
	public class NewsSchema : Schema
	{
		public NewsSchema(IDependencyResolver resolver) : base(resolver)
		{
			Query = resolver.Resolve<NewsQuery>();
			Mutation = resolver.Resolve<NewsMutation>();
		}
	}

}
