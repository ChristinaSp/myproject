﻿using GraphQL.Types;
using NewsGraphQl.Database;
using NewsGraphQl.Graphql.Type;
using System.Linq;

namespace NewsGraphQl.Graphql
{
	public class NewsQuery : ObjectGraphType
	{
		public NewsQuery(INewsRepository repository)
		{
			Field<ListGraphType<NewsType>>("getNews", resolve: context => repository.Get());

			Field<NewsType>("getNewsById",
				arguments: new QueryArguments(new QueryArgument<IdGraphType> { Name = "id" }),
				resolve: context =>
				{
					var id = context.GetArgument<string>("id");
					return repository.Get().FirstOrDefault(x => x.Id == id);
				});
		}
	}

}
