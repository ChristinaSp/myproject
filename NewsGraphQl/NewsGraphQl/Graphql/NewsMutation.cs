﻿using GraphQL.Types;
using NewsGraphQl.Database;
using NewsGraphQl.Graphql.Type;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace NewsGraphQl.Graphql
{
	public class NewsMutation : ObjectGraphType
	{
		public NewsMutation(INewsRepository repository)
		{
            Field<NewsType>("feedback",
                 arguments: new QueryArguments(new QueryArgument<NonNullGraphType<NewsInputType>> { Name = "news" },
                            new QueryArgument<NonNullGraphType<IdGraphType>> { Name = "newId" }),
                 resolve: context =>
                 {
                     var news = context.GetArgument<News>("news");
                     var newsId = context.GetArgument<string>("newId");
                     var dbOwner = repository.GetBuId(newsId);
                     return repository.Update(newsId, news);
                 });

         
        }
    }


}
