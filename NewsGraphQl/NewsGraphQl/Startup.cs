
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;


using GraphQL;
using GraphQL.Server;
using GraphQL.Server.Ui.Playground;
using GraphQL.Types;
using GraphiQl;
using NewsGraphQl.Graphql.Type;
using NewsGraphQl.Graphql;
using NewsGraphQl.Database;
using AutoMapper;

namespace NewsGraphQl
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<NewsType>();
            services.AddSingleton<NewsInputType>();

            services.AddScoped<IDependencyResolver>(s => new
                    FuncDependencyResolver(s.GetRequiredService));
            services.AddScoped<NewsSchema>();
            services.AddGraphQL().AddGraphTypes(ServiceLifetime.Scoped);
         

            services.AddCors(o => o.AddPolicy("WebApiPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddMvc().AddNewtonsoftJson();
            services.AddMvc(option => option.EnableEndpointRouting = false);
           

            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddScoped<INewsRepository>(_ => new NewsRepository());
            var spp = services.BuildServiceProvider();
            services.AddSingleton<ISchema>(new NewsSchema(new FuncDependencyResolver(type => spp.GetService(type))));

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.UseRouting();

            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapGet("/", async context =>
            //    {
            //        await context.Response.WriteAsync("Hello World!");
            //    });
            //});
            app.UseCors("WebApiPolicy");

           // app.UseHttpsRedirection();
            app.UseMvc();

           // app.UseGraphiQl<NewsSchema>();
          
            app.UseGraphiQl("/graphql");

            app.UseGraphQLPlayground
               (new GraphQLPlaygroundOptions());


        }
    }
}
