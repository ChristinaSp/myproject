﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsGraphQl.Database
{
    public class AppDBContext
    {
        private readonly IMongoDatabase _database;

        public AppDBContext()
        {
            var client = new MongoClient("mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");
            var database = client.GetDatabase("New");
        }


        public IMongoCollection<News> News => _database.GetCollection<News>("News");

    }
}
