﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsGraphQl.Database
{
    public interface INewsRepository
    {
        public List<News> Get();

        public News GetBuId(string id);

        public News Create(News news);

        public List<News> CreateManu(List<News> news);

        public News Update(string id, News newsIn);

        public void Remove(News newsIn);

        public void Remove(string id);

    }
}
