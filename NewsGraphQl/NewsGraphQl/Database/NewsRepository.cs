﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NewsGraphQl.Database
{
    public class NewsRepository: INewsRepository
    {
        private readonly IMongoCollection<News> _news;

        public NewsRepository()
        {
            var client = new MongoClient("mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");
            var database = client.GetDatabase("New");

            _news = database.GetCollection<News>("news");
        }
        public List<News> Get() =>  _news.Find(news => true).ToList();

        public News GetBuId(string id) =>  _news.Find<News>(news => news.Id == id).FirstOrDefault();

        public News Create(News news)
        {
            _news.InsertOne(news);
            return news;
        }

        public List<News> CreateManu(List<News> news)
        {
          _news.InsertMany(news);
            return news;
        }

        public News Update(string id, News newsIn)
        {
            _news.ReplaceOne(news => news.Id == id, newsIn);
            return newsIn;
        }

        public void Remove(News newsIn) =>
            _news.DeleteOne(book => book.Id == newsIn.Id);

        public void Remove(string id) =>
            _news.DeleteOne(news => news.Id == id);
     
    }
}
