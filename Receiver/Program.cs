﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Receiver
{
    class Program
    {

        private static void WriteToDataBase(List<News> news)
        {
            MongoClient dbClient = new MongoClient("mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false");
            var database = dbClient.GetDatabase("New");
            var collection = database.GetCollection<BsonDocument>("news");


            foreach (var item in news)
            {

                var document = new BsonDocument() {
                    { "title", item.Title },
                    {"time", item.Time },
                    {"link", item.Link },
                };
                collection.InsertOne(document);
            }
        }
        static void Main(string[] args)
        {
            List<News> news = null;

            var factory = new ConnectionFactory() { HostName = "localhost" };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "hello",
                                     durable: false,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                var consumer = new  EventingBasicConsumer(channel);
                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    news = JsonConvert.DeserializeObject<List<News>>(message);
                    foreach (var item in news)
                    {
                        Console.WriteLine(item);
                    }
                    if (news != null)
                        WriteToDataBase(news);


                };
                channel.BasicConsume(queue: "hello",
                                     autoAck: true,
                                     consumer: consumer);


                Console.WriteLine(" Press [enter] to exit.");
                Console.ReadLine();
            }

        }
    }
}
