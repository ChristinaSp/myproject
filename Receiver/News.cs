﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Receiver
{
    public class News
    {
        private string _title;
        private DateTime _time;
        private string _link;
        public News(string title, DateTime time, string link)
        {
            Time = time;
            Title = title;
            Link = link;
        }

        public string Title { get => _title; set => _title = value; }
        public DateTime Time { get => _time; set => _time = value; }
        public string Link { get => _link; set => _link = value; }

        public override string ToString()
        {
            return $"{Title} - {Time}\n{Link}\n";
        }
    }
}
