﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 
using GraphQL.Common.Request;
using GraphQL.Client;

namespace WpfClient
{
    public class NewsService
    {
        protected readonly  GraphQLClient _client;
        private readonly string _url = "https://localhost:44343/api/news";

        public NewsService()
        {
            _client = new GraphQLClient(_url);
        }

        public List<News> GetAllNews()
        {
            var request = new GraphQLRequest()
            {
                Query = @"
                    query { 
                        getNews { 
                            id
                            title
                            link
                            time
                            like
                        }
                    }"
            };
            var graphQLResponse = _client.PostAsync(request).Result;
            var news = graphQLResponse.GetDataFieldAs<List<News>>("getNews");
            return news;
        }

        public bool Feedback(News news, bool like)
        {
            news.Like = like;
            var request = new GraphQLRequest()
            {
                Query = @"
                    mutation ($news: NewsInput!, $newsId:ID!)
                      {
                        feedback(news:$news, newId:$newsId)
                            { 
                              id
                              title
                              link
                              time
                              like        }
                        }",
                Variables = new { news = news, newsId = news.ID }
            };
            var graphQLResponse = _client.PostAsync(request).Result;
            var infoo = graphQLResponse.GetDataFieldAs<News>("feedback");
            return infoo.Like;
        }
    }
}
