﻿//using DocumentFormat.OpenXml.Presentation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfClient
{
    public class News : INotifyPropertyChanged
    {
        public News()
        {
         //   Comments = new List<Comment>();
        }
        public string ID { get; set; }
        public string Title { get; set; } 
        public DateTime Time { get; set; }
        public string Link { get; set; } 

        //public bool IsLikeUnset => !Like.HasValue;
        //public bool IsLikeSet => Like.HasValue;

       // public string LikeString => Like.HasValue ? Like.Value ? "OK" : "Not OK" : "";

        private bool _like { get; set; }
        public bool Like
        {
            get
            {
                return _like;
            }
            set
            {
                _like = value;
                OnPropertyChanged("Like");
            }
        }
        //public List<Comment> Comments { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            //return string.Format("{0}{1}Title: {2}{1}Link: {3}{1}Description: {4}{1}Time: {5}{1}Comments: {6}{1}{0}",
            //    "----------------------------------------",
            //    Environment.NewLine,
            //    Title,
            //    Link, 
            //    DateOfPublication.ToLongTimeString(),
            //    Comments.Count);

            return $"Title: {Title} Link: {Link} Time: {Time}";
        }
    }
}

