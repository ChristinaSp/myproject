﻿using SkyMVVM;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace WpfClient
{
    public class NewsListViewModel : INotifyPropertyChanged
    {
        private NewsService _newsService { get; set; }
        private ObservableCollection<News> _newsList;
        private ICommand _savecommand;

        public ObservableCollection<News> NewsList
        {
            get { return _newsList; }
            set
            {
                _newsList = value;
                OnPropertyChanged(nameof(NewsList));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }

        public NewsListViewModel()
        {
            _newsService = new NewsService();
            Update();
        }

        public void Update()
        {
            var news = _newsService.GetAllNews();
            NewsList = new ObservableCollection<News>(news);
        }
        //public ICommand SaveCommand => _savecommand ?? (_savecommand = new RelayCommand((param) => Like());

        public void Like(News news, bool like)
        {
            bool newLike = _newsService.Feedback(news, like);

            var updatedItem = NewsList.Single(x => x.ID == news.ID);
            updatedItem.Like = newLike;
        }
    }
}
