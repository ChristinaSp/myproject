﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace WpfClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class InfoWindow : Window
    {
        NewsListViewModel _viewModel;
        CollectionViewSource _newsCollection;

        public InfoWindow()
        { 
            InitializeComponent();
            _viewModel = new NewsListViewModel();
              DataContext = _viewModel;


            _newsCollection = (CollectionViewSource)(Resources["NewsCollection"]);
            _newsCollection.Filter += _newsCollection_Filter;
        }
       
        private void _newsCollection_Filter(object sender, FilterEventArgs e)
        {
            var filter = txtFilter.Text;
            var news = e.Item as News;
            if (news.Title.Contains(filter) || (!string.IsNullOrWhiteSpace(news.Title) && news.Title.Contains(filter)))
            {
                e.Accepted = true;
            }
            else
            {
                e.Accepted = false;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _newsCollection.View.Refresh();
        }

        private void FeedbackClick(News news, bool like)
        {
            _viewModel.Like(news, like);
        }

        private void Like_Click(object sender, RoutedEventArgs e)
        {
            var n = ((FrameworkElement)sender).DataContext as News;
            FeedbackClick(n, true);
        }

        private void Dislike_Click(object sender, RoutedEventArgs e)
        {
            var n = ((FrameworkElement)sender).DataContext as News;
            FeedbackClick(n, false);
        }
    }
}
